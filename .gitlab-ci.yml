# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
stages:
- dependencies
- test
- gitlab-scans
- snyk
- release

variables:
  SAST_EXCLUDED_PATHS: "spec, test, tests, tmp, coverage"

sast:
  stage: gitlab-scans

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml

### Node boot strap

.node:
  variables:
    IMAGE: "node:16"
  image: $IMAGE
  before_script:
    - echo "Setting up base image..."

.node_cache:
  extends: .node
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - node_modules/
    policy: pull

.dependencies:
  extends: .node
  cache:
    key: $CI_COMMIT_REF_SLUG-$CI_PROJECT_DIR
    paths:
      - node_modules/
  script:
    - npm ci
  rules:
    - if: '$CI_MERGE_REQUEST_ID == null'
      changes:
        - package-lock.json

dependency_scanning:
  extends: .node_cache
  stage: snyk
  script:
    - echo "$CI_JOB_NAME is used for configuration only, and its script should not be executed"
    - exit 1
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json
  dependencies: []
  rules:
    - when: never

.snyk-analyzer:
  extends: dependency_scanning
  allow_failure: true
  # `rules` must be overridden explicitly by each child job
  # see https://gitlab.com/gitlab-org/gitlab/-/issues/218444
  before_script:
    - npm install -g npm@latest
    # Install npm, snyk, and snyk-to-html
    - npm install -g snyk snyk-to-html
    # Run snyk help, snyk auth, snyk monitor, snyk test to break build and out report
    - snyk --help
    - snyk auth $SNYK_TOKEN
    # This doesn't seem smart.
    # - snyk test --org=$SNYK_ORG --json --json-file-output=snyk_results.json | snyk-to-html -o snyk_results.html
    # Cache isnt working due to k8s runners
    - npm ci
    - snyk monitor --org=$SNYK_ORG

snyk-dependency_scanning:
  image: node:16
  variables:
    VALIDATE_SCHEMA: "true"

  stage: snyk
  script:
    - snyk test --org=$SNYK_ORG --json --json-file-output=snyk_results.json | bin/snyk2gitlab

  extends: .snyk-analyzer
  # Save report to artifacts
  rules:
    - if: '$SNYK_DEPENDENCY_DISABLED'
      when: never
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
    - if: '$CI_MERGE_REQUEST_ID == null'

snyk-dependency_scanning_html:
  image: node:16
  variables:
    VALIDATE_SCHEMA: "true"

  stage: snyk
  script:
    - snyk test --org=$SNYK_ORG --json --json-file-output=snyk_results.json | snyk-to-html -o snyk_results.html

    # Save report to artifacts
  artifacts:
    when: always
    paths:
      - snyk_results.html

  extends: .snyk-analyzer
  # Save report to artifacts
  rules:
    - if: '$SNYK_DEPENDENCY_DISABLED'
      when: never
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
    - if: '$CI_MERGE_REQUEST_ID == null'

### Stage Jobs

Install Dependencies:
  extends: .dependencies
  stage: dependencies

Node Lint:
  stage: test
  extends: .node_cache
  script:
    # Cache isnt working due to k8s runners
    - npm ci
    - npm run lint

Node Coverage:
  stage: test
  extends: .node_cache
  script:
    # Cache isnt working due to k8s runners
    - npm ci
    - npm run coverage
  artifacts:
    reports:
      junit: junit.xml
      cobertura: coverage/cobertura-coverage.xml
    paths:
      - coverage/

retire-js-dependency_scanning:
  stage: gitlab-scans

gemnasium-dependency_scanning:
  stage: gitlab-scans

release:
  image: node:latest
  stage: release
  before_script:
    - npm ci --cache .npm --prefer-offline
    #- |
    #  {
    #    echo "@${CI_PROJECT_ROOT_NAMESPACE}:registry=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/"
    #    echo "${CI_API_V4_URL#https?}/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=\${CI_JOB_TOKEN}"
    #  } | tee --append .npmrc
  script:
    - npx semantic-release
  only:
    - main
