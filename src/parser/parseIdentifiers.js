function parseIdentifiers(identifiersObj) {
    const identifiersSource = new Array();
    // I think there's a better way to do this... I am sorry
    for (const i in identifiersObj) {
        for (const x in identifiersObj[i]) {
            identifiersSource.push(identifiersObj[i][x]);
        }
    }
    const identifiersObjTransform = identifiersSource.map(v => ({
            type: 'snyk',
            name: 'snyk'.concat('-', v),
            value: v,
            url: 'https://snyk.io/'
        }));
    return identifiersObjTransform;
}
module.exports = parseIdentifiers;
